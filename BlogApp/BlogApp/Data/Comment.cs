﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Data
{
    public class Comment : BaseModel
    {
        [Required]
        [MaxLength(200,ErrorMessage = "Tên dài quá 200 ký tự")]
        public string Sender { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Địa chỉ email không hợp lệ")]
        public string Email { get; set; }

        [Required]
        public int ArticleId { get; set; }

        [MinLength(10, ErrorMessage = "Lời nhắn quá ngắn")]
        public string Content { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Article Article { get; set; }
    }
}
