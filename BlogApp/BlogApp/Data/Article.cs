﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Data
{
    public class Article : BaseModel
    {
        public int UserId { get; set; }

        public string Content { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public virtual IEnumerable<Comment> Comments { get; set; }
    }
}
