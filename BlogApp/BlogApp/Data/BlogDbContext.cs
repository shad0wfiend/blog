﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Data
{
    public class BlogDbContext : DbContext
    {
        public BlogDbContext(DbContextOptions<BlogDbContext> options) : base(options)
        {

        }
        
        DbSet<User> Users { get; set; }
        DbSet<Article> Articles { get; set; }
        DbSet<Comment> Comments { get; set; }
    }
}
