﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Data
{
    public class User : BaseModel
    {
        [MaxLength(200, ErrorMessage = "Tên không được quá 200 ký tự")]
        [RegularExpression(@"^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$",
            ErrorMessage = "Tên người dùng không hợp lệ")]
        public string UserName { get; set; }

        [MaxLength(50, ErrorMessage = "Tên không được quá 50 ký tự")]
        public string FullName { get; set; }

        [MinLength(5, ErrorMessage = "Mật khẩu phải dài hơn 5 ký tự")]
        public string Password { get; set; }

        public virtual IEnumerable<Article> Articles { get; set; }
    }
}
